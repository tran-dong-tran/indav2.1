from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtWidgets
import sys
import cv2
import os
from alter_contrast import contrast

class MainApp(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Create data')
        path = QDir.currentPath() + '/INDA_data'
        self.source = QDir.toNativeSeparators(path)
        self.dest = self.source
        self.shades = 0
        self.degree = 0
        self.noise = 0
        self.setupUi(self)
        self.image = None
        self.processedImage = None
        self.folderName = ""
        self.chooseButtonSource.clicked.connect(self.chooseSourcePath)
        self.chooseButtonDest.clicked.connect(self.chooseDestPath)
        self.alterAddNumButton.clicked.connect(self.addNumContrast)
        self.alterSubNumButton.clicked.connect(self.subNumContrast)
        self.alterAddDegButton.clicked.connect(self.addDegContrast)
        self.alterSubDegButton.clicked.connect(self.subDegContrast)
        self.addNoiseButton.clicked.connect(self.addNoise)
        self.subNoiseButton.clicked.connect(self.subNoise)
        self.startAlter.clicked.connect(self.alterImages)
        self.trainTxt.clicked.connect(self.createTrainTxt)

    ###
    # User Interface
    ###

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(100, 50)
        MainWindow.setMaximumSize(QSize(400, 150))
        self.showMaximized()
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.alterNumLayout = QHBoxLayout()
        self.alterNumLayout.setObjectName("alterNumBox")

        self.alterDegLayout = QHBoxLayout()
        self.alterDegLayout.setObjectName("alterDegBox")

        self.noiseLayout = QHBoxLayout()
        self.noiseLayout.setObjectName("alterDegBox")

        self.sourceBox = QLineEdit(self)
        self.sourceBox.setText(self.source)
        self.sourceBox.setReadOnly(True)
        self.sourceBox.move(20, 20)
        self.sourceBox.resize(280, 40)
        self.sourceLabel = QLabel("Source:                                                                              *")

        self.destBox = QLineEdit(self)
        self.destBox.setText(self.source)
        self.destBox.setReadOnly(True)
        self.destBox.move(20, 20)
        self.destBox.resize(280, 40)
        self.destLabel = QLabel("Destination:")

        self.chooseButtonSource = QPushButton(self.centralwidget)
        self.chooseButtonSource.setText('Choose...')
        self.chooseButtonDest = QPushButton(self.centralwidget)
        self.chooseButtonDest.setText('Choose...')

        self.alterNumBox = QLineEdit(self)
        self.alterNumBox.move(20, 20)
        self.alterNumBox.resize(280, 40)
        self.alterNumBox.setText(str(self.shades))
        self.alterAddNumButton = QPushButton(self)
        self.alterAddNumButton.setText('+')
        self.alterSubNumButton = QPushButton(self)
        self.alterSubNumButton.setText('-')
        self.alterNumLabel = QLabel('Brightness count:')

        self.alterDegBox = QLineEdit(self)
        self.alterDegBox.move(20, 20)
        self.alterDegBox.resize(280, 40)
        self.alterDegBox.setText(str(self.degree))
        self.alterAddDegButton = QPushButton(self)
        self.alterAddDegButton.setText('+')
        self.alterSubDegButton = QPushButton(self)
        self.alterSubDegButton.setText('-')
        self.alterDegLabel = QLabel('Brightness degree:')

        self.noiseBox = QLineEdit(self)
        self.noiseBox.move(20, 20)
        self.noiseBox.resize(280, 40)
        self.noiseBox.setText(str(self.noise))
        self.addNoiseButton = QPushButton(self)
        self.addNoiseButton.setText('+')
        self.subNoiseButton = QPushButton(self)
        self.subNoiseButton.setText('-')
        self.noiseLabel = QLabel('Noise degree:')

        self.startAlter = QPushButton(self.centralwidget)
        self.startAlter.setLayoutDirection(Qt.LeftToRight)
        self.startAlter.setObjectName("startAlter")
        self.startAlter.setText('Create images')

        self.trainTxt = QPushButton(self.centralwidget)
        self.trainTxt.setLayoutDirection(Qt.RightToLeft)
        self.trainTxt.setObjectName("trainTxt")
        self.trainTxt.setText('Create train.txt')

        self.imgLabel = QLabel(self.centralwidget)
        self.imgLabel.setMaximumSize(QSize(940, 800))

        font = QFont()
        font.setPointSize(7)
        font.setBold(False)
        font.setWeight(50)
        self.imgLabel.setFont(font)
        self.imgLabel.setText("")
        self.imgLabel.setObjectName("imgLabel")
        self.horizontalLayout.addWidget(self.imgLabel)

        self.processedLabel = QLabel(self.centralwidget)
        self.processedLabel.setMaximumSize(QSize(940, 800))
        self.processedLabel.setAlignment(Qt.AlignCenter)
        self.processedLabel.setObjectName("processedLabel")

        self.horizontalLayout.addWidget(self.processedLabel)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.verticalLayout.addWidget(self.sourceLabel)
        self.verticalLayout.addWidget(self.sourceBox)
        self.verticalLayout.addWidget(self.chooseButtonSource)

        self.verticalLayout.addWidget(self.destLabel)
        self.verticalLayout.addWidget(self.destBox)
        self.verticalLayout.addWidget(self.chooseButtonDest)

        self.verticalLayout.addLayout(self.alterNumLayout)
        self.alterNumLayout.addWidget(self.alterNumLabel)
        self.alterNumLayout.addWidget(self.alterNumBox)
        self.alterNumLayout.addWidget(self.alterAddNumButton)
        self.alterNumLayout.addWidget(self.alterSubNumButton)

        self.verticalLayout.addLayout(self.alterDegLayout)
        self.alterDegLayout.addWidget(self.alterDegLabel)
        self.alterDegLayout.addWidget(self.alterDegBox)
        self.alterDegLayout.addWidget(self.alterAddDegButton)
        self.alterDegLayout.addWidget(self.alterSubDegButton)

        self.verticalLayout.addLayout(self.noiseLayout)
        self.noiseLayout.addWidget(self.noiseLabel)
        self.noiseLayout.addWidget(self.noiseBox)
        self.noiseLayout.addWidget(self.addNoiseButton)
        self.noiseLayout.addWidget(self.subNoiseButton)

        self.verticalLayout.addWidget(self.startAlter)
        self.verticalLayout.addWidget(self.trainTxt)
        MainWindow.setCentralWidget(self.centralwidget)

        QMetaObject.connectSlotsByName(MainWindow)

    ###
    # Button control
    # Following methods set the input values of all fields
    ###

    # source: from which directory images are loaded
    # dest(ination): to which directory new images are saved
    # shades: number of requested images with altered brightness in each direction (lighter/darker)
    # degree: degree of brightness alteration
    # noise: degree of noise alteration

    def chooseSourcePath(self):
        path = QtWidgets.QFileDialog.getExistingDirectory(self, 'Choose source directory...', str(self.source))
        if path:
            self.source = path
            self.sourceBox.setText(self.source)
            if self.dest == '':
                self.dest = self.source
                self.destBox.setText(self.source)

    def chooseDestPath(self):
        path = QtWidgets.QFileDialog.getExistingDirectory(self, 'Choose destination directory...', str(self.dest))
        if path:
            self.dest = path
            self.destBox.setText(self.dest)

    def addNumContrast(self):
        # limit to 9 as to not make computers explode
        if self.shades == 9:
            return
        else:
            self.shades += 1
            self.alterNumBox.setText(str(self.shades))

    def subNumContrast(self):
        # don't allow negative shades, since we create either 0 or more brightness altered images
        if self.shades == 0:
            return
        else:
            self.shades -= 1
            self.alterNumBox.setText(str(self.shades))

    def addDegContrast(self):
        self.degree += 10
        self.alterDegBox.setText(str(self.degree))

    def subDegContrast(self):
        # don't allow negative values, since we create shades in both brightness directions already
        if self.degree == 0:
            return
        else:
            self.degree -= 10
            self.alterDegBox.setText(str(self.degree))

    def addNoise(self):
        self.noise += 10
        self.noiseBox.setText(str(self.noise))

    def subNoise(self):
        # don't allow negative values since we can't add negative noise
        if self.noise == 0:
            return
        else:
            self.noise -= 10
            self.noiseBox.setText(str(self.noise))

    ###
    # Helper method
    ###

    # checks if given directories are set
    # @return bool
    def directoriesValid(self):
        if self.source == '' or self.dest == '':
            print('Directories invalid...')
            return False
        else:
            return True

    ###
    # Logic
    # Following methods implement the logic of image alteration
    ###

    # reads out all files and saves altered images, depending on current input values
    #
    # Currently there are following cases, producing 'total:' amount of images in dest directory per source image
    # a) if everything == 0: only copy over files from source to dest (total: 1)
    # b) if noise != 0 && shades/degree == 0: do a) + save a noisy image (total: 2)
    # c) if noise == 0 && shades/degree != 0: do a) + save images with altered brightness in each direction (total: 1 + 2*shades)
    # d) if everything != 0: do a) and save images with altered noise and brightness (total: 2 + 2*shades)
    # @return none
    def alterImages(self):
        if not self.directoriesValid():
            return

        print('Getting all .jpg in dir...')
        filePaths = []
        for file in os.listdir(self.source):
            if file.endswith(".jpg"):
                filePaths.append(file)

        print('Processing images...')
        # noisy image
        for path in filePaths:
            baseDestPath = os.path.splitext(os.path.join(self.dest, path))[0]
            newDestPath = baseDestPath
            org = cv2.imread(os.path.join(self.source, path))
            new = [newDestPath, org]
            print('Working on "' + newDestPath + '" ', end='', flush=True)
            cv2.imwrite(new[0] + '.jpg', new[1])  # save original image too

            # adding noise, save original noise image
            if self.noise != 0:
                print('[adding noise: ' + str(self.noise) + ']', end='', flush=True)
                new[0] += '-noise-' + str(self.noise)
                new[1] = contrast.add_noise(new[1], 0.0, self.noise)
                cv2.imwrite(new[0] + '.jpg', new[1])

            new = [newDestPath, org]

            # adding and subtracting brightness to image
            if self.shades > 0 and self.degree > 0:
                print('[adding shades:', end='', flush=True)
                for i in range(self.shades):
                    current_brightness = self.degree * (i + 1)
                    hsv = cv2.cvtColor(new[1], cv2.COLOR_BGR2HSV)
                    new_bright = cv2.cvtColor(contrast.change_brightness(hsv, current_brightness), cv2.COLOR_HSV2BGR)
                    new_dark = cv2.cvtColor(contrast.change_brightness(hsv, -current_brightness), cv2.COLOR_HSV2BGR)

                    print(' +' + str(current_brightness), end='', flush=True)
                    cv2.imwrite(new[0] + '-plus-' + str(current_brightness) + '.jpg', new_bright)

                    print(' -' + str(current_brightness), end='', flush=True)
                    cv2.imwrite(new[0] + '-minus-' + str(current_brightness) + '.jpg', new_dark)
                print(']', end='', flush=True)
            print(' [DONE]')
        print('Creating images [DONE]')

    # reads out all .jpg files and writes their paths into train.txt in the destination directory
    # @return none
    def createTrainTxt(self):
        print('Getting all .jpg in dir...')
        filePaths = []
        f = open(self.dest + '/' + 'train.txt', 'w')
        for file in os.listdir(self.dest):
            if file.endswith(".jpg"):
                # print(os.path.join(self.dest, file))
                f.write(os.path.join(self.dest, file) + '\n')
        print('Creating train.txt [DONE]')

# Defining main function to be run
def main():
    # Initializing instance of Qt Application
    app = QtWidgets.QApplication(sys.argv)

    # Initializing object of designed GUI
    window = MainApp()

    # Showing designed GUI
    # window.show()

    # Running application
    app.exec_()

# Checking if current namespace is main and file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()