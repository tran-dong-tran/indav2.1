Ergebnisse INDA Gladbach, Theml, Tran


______________________________________________________________________________________

Lenas Training (create_data Eingabe 2-10-0; Daten: 20200526_HeatPA191113200129, 20200701_SNOMAX_500nm_n0)


best weights, val mit 20200703_SNOMAX_800nm_n1_090 (92)
	detections_count = 9548, unique_truth_count = 8736  
	class_id = 0, name = liquid, ap = 99.47%   	 (TP = 7514, FP = 267) 
	class_id = 1, name = frozen, ap = 88.10%   	 (TP = 892, FP = 166) 

 	for conf_thresh = 0.25, precision = 0.95, recall = 0.96, F1-score = 0.96 
 	for conf_thresh = 0.25, TP = 8406, FP = 433, FN = 330, average IoU = 86.15 % 

 	IoU threshold = 50 %, used Area-Under-Curve for each unique Recall 
 	mean average precision (mAP@0.50) = 0.937838, or 93.78 % 
	
	
best weights, val mit 20200608_MilliQ, 20210122_Alg_1g_l_INDA2 (436)
	detections_count = 47565, unique_truth_count = 41568  
	class_id = 0, name = liquid, ap = 99.73%   	 (TP = 33041, FP = 218) 
	class_id = 1, name = frozen, ap = 97.22%   	 (TP = 7862, FP = 784) 

 	for conf_thresh = 0.25, precision = 0.98, recall = 0.98, F1-score = 0.98 
 	for conf_thresh = 0.25, TP = 40903, FP = 1002, FN = 665, average IoU = 83.36 % 

 	IoU threshold = 50 %, used Area-Under-Curve for each unique Recall 
 	mean average precision (mAP@0.50) = 0.984770, or 98.48 % 


best weights, val mit 20200608_MilliQ inkl 2-10-10 (1836)
	detections_count = 182714, unique_truth_count = 176256  
	class_id = 0, name = liquid, ap = 99.95%   	 (TP = 145983, FP = 187) 
	class_id = 1, name = frozen, ap = 99.87%   	 (TP = 29777, FP = 884) 

 	for conf_thresh = 0.25, precision = 0.99, recall = 1.00, F1-score = 1.00 
 	for conf_thresh = 0.25, TP = 175760, FP = 1071, FN = 496, average IoU = 89.44 % 

 	IoU threshold = 50 %, used Area-Under-Curve for each unique Recall 
 	mean average precision (mAP@0.50) = 0.999069, or 99.91 % 

	
______________________________________________________________________________________
	
Dongs Training (create_data Eingabe 2-10-10; Daten: 20200525_MilliQ, 20200609_MilliQ,  20200701_SNOMAX_500nm_n5, 20200708_SNOMAX_800nm_40l_n5, 20200703_SNOMAX_800nm_n5)


best weights, val mit 20200703_SNOMAX_800nm_n1 (92)
	detections_count = 10683, unique_truth_count = 8736  
	class_id = 0, name = liquid, ap = 99.32%   	 (TP = 7643, FP = 356) 
	class_id = 1, name = frozen, ap = 88.72%   	 (TP = 834, FP = 22) 

 	for conf_thresh = 0.25, precision = 0.96, recall = 0.97, F1-score = 0.96 
 	for conf_thresh = 0.25, TP = 8477, FP = 378, FN = 259, average IoU = 84.70 % 

 	IoU threshold = 50 %, used Area-Under-Curve for each unique Recall 
 	mean average precision (mAP@0.50) = 0.940180, or 94.02 % 
 	
 	
best weights, val mit 20200608_MilliQ, 20210122_Alg_1g_l_INDA2 (436)
	detections_count = 53191, unique_truth_count = 41568  
	class_id = 0, name = liquid, ap = 99.91%   	 (TP = 33227, FP = 8) 
	class_id = 1, name = frozen, ap = 99.92%   	 (TP = 8170, FP = 670) 

 	for conf_thresh = 0.25, precision = 0.98, recall = 1.00, F1-score = 0.99 
 	for conf_thresh = 0.25, TP = 41397, FP = 678, FN = 171, average IoU = 89.25 % 

 	IoU threshold = 50 %, used Area-Under-Curve for each unique Recall 
 	mean average precision (mAP@0.50) = 0.999138, or 99.91 %  
 	
 	
best weights, val mit 20200608_MilliQ inkl 2-10-10 (1836)
	detections_count = 235864, unique_truth_count = 176256  
	class_id = 0, name = liquid, ap = 99.98%   	 (TP = 146294, FP = 42) 
	class_id = 1, name = frozen, ap = 99.89%   	 (TP = 29838, FP = 3018) 

 	for conf_thresh = 0.25, precision = 0.98, recall = 1.00, F1-score = 0.99 
 	for conf_thresh = 0.25, TP = 176132, FP = 3060, FN = 124, average IoU = 88.93 % 

 	IoU threshold = 50 %, used Area-Under-Curve for each unique Recall 
 	mean average precision (mAP@0.50) = 0.999382, or 99.94 % 
	

______________________________________________________________________________________

Hungs Trainig fortgesetzt mit Daten wie bei Dongs Training


best weights, val mit 20200703_SNOMAX_800nm_n1 (92)
	detections_count = 9115, unique_truth_count = 8736  
	class_id = 0, name = liquid, ap = 99.92%   	 (TP = 7645, FP = 172) 
	class_id = 1, name = frozen, ap = 94.64%   	 (TP = 982, FP = 13) 

 	for conf_thresh = 0.25, precision = 0.98, recall = 0.99, F1-score = 0.98 
 	for conf_thresh = 0.25, TP = 8627, FP = 185, FN = 109, average IoU = 90.78 % 

 	IoU threshold = 50 %, used Area-Under-Curve for each unique Recall 
 	mean average precision (mAP@0.50) = 0.972818, or 97.28 % 
 	

best weights, val mit 20200608_MilliQ, 20210122_Alg_1g_l_INDA2 (436)
 	detections_count = 44519, unique_truth_count = 41568  
	class_id = 0, name = liquid, ap = 99.98%   	 (TP = 33384, FP = 113) 
	class_id = 1, name = frozen, ap = 99.49%   	 (TP = 8100, FP = 11) 

 	for conf_thresh = 0.25, precision = 1.00, recall = 1.00, F1-score = 1.00 
 	for conf_thresh = 0.25, TP = 41484, FP = 124, FN = 84, average IoU = 92.64 % 

 	IoU threshold = 50 %, used Area-Under-Curve for each unique Recall 
 	mean average precision (mAP@0.50) = 0.997362, or 99.74 % 
 	

best weights, val mit 20200608_MilliQ inkl 2-10-10 (1836)
	detections_count = 181759, unique_truth_count = 176256  
	class_id = 0, name = liquid, ap = 100.00%   	 (TP = 146376, FP = 45) 
	class_id = 1, name = frozen, ap = 99.87%   	 (TP = 29838, FP = 21) 

 	for conf_thresh = 0.25, precision = 1.00, recall = 1.00, F1-score = 1.00 
 	for conf_thresh = 0.25, TP = 176214, FP = 66, FN = 42, average IoU = 94.06 % 

 	IoU threshold = 50 %, used Area-Under-Curve for each unique Recall 
 	mean average precision (mAP@0.50) = 0.999348, or 99.93 % 


