from pathlib import Path
import pickle
# initialize variables for storage
fixPos = []
folderPath = ""
imageFilename = []
difference = []

frozenNewID = []
imagePath = []
results = []
resultsOriginal=[]
warningBox= []
resultsWarning = []
refinedResults = []
oldProgramFrozenNewID = []
root = Path(".")

# get variables from gui-yolo3-muti.py
def storeFixPos(input):
    global fixPos
    fixPos = input
    outputFile = 'fixPos.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(fixPos, fw)
    fw.close()

# folder_path
def storeFolderPath(input):
    global folderPath
    folderPath = input
    outputFile = 'folderPath.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(folderPath, fw)
    fw.close()

def storeFrozenNewID(input):
    global frozenNewID
    frozenNewID = input
    outputFile = 'frozenNewID.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(frozenNewID, fw)
    fw.close()

def storeImagePath(input):
    global imagePath
    imagePath = input
    outputFile = 'imagePath.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(imagePath, fw)
    fw.close()

def storeResults(input):
    global results
    results = input
    outputFile = 'results.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(results, fw)
    fw.close()

def storeResultsOriginal(input):
    global resultsOriginal
    resultsOriginal = input
    outputFile = 'resultsOriginal.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(resultsOriginal, fw)
    fw.close()

def storeWarningBox(input):
    global warningBox
    warningBox = input
    outputFile = 'warningBox.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(warningBox, fw)
    fw.close()

def storeResultsWarning(input):
    global resultsWarning
    resultsWarning = input
    outputFile = 'resultsWarning.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(resultsWarning, fw)
    fw.close()

def storeRefinedResults(input):
    global refinedResults
    refinedResults = input
    outputFile = 'refinedResults.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(refinedResults, fw)
    fw.close()

def storeImageFilename(input):
    global imageFilename
    imageFilename = input
    outputFile = 'imageFilename.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(imageFilename, fw)
    fw.close()

def storeDifference(input):
    global difference
    difference = input
    outputFile = 'difference.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(difference, fw)
    fw.close()

def storeOldProgramFrozenNewID(input):
    global oldProgramFrozenNewID
    oldProgramFrozenNewID = input
    outputFile = 'oldProgramFrozenNewID.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(oldProgramFrozenNewID, fw)
    fw.close()


