import os
import pickle
from pathlib import Path
from storage import storeRefinedResults
import detection
import oldProgram
import drawBoundingBox
import boundaryCondition

root = Path(".")
updatedResults = []
originalResults = []
refinedResults = []
imageFilename = []
oldProgramFrozenNewID = []
folderPath = ""


def loadVariables():
    global updatedResults
    global originalResults
    global oldProgramFrozenNewID
    global imageFilename
    global folderPath


    inputFile = 'resultsWarning.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    updatedResults = pickle.load(fd)

    inputFile = 'results.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    originalResults = pickle.load(fd)

    inputFile = 'oldProgramFrozenNewID.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    oldProgramFrozenNewID = pickle.load(fd)


    inputFile = 'imageFilename.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    imageFilename = pickle.load(fd)

    inputFile = 'folderPath.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    folderPath = pickle.load(fd)

#remove class = 2 after Update Button is clicked
#save as refinedResult
def refineWarningData():
    global updatedResults
    global originalResults
    global refinedResults

    refinedResults = updatedResults
    for i in range(len(refinedResults)):
        for j in range(len(refinedResults[i])):
            for k in range(len(refinedResults[i][j])):
                if refinedResults[i][j][k][4] == 2:
                    temp = list(refinedResults[i][j][k])
                    temp[4] = originalResults[i][j][k][4]
                    refinedResults[i][j][k] = tuple(temp)
    storeRefinedResults(refinedResults)

def recalulateVariables():
    global folderPath
    global imageFilename
    global refinedResults

    # get the current working dir
    src_dir = os.getcwd()
    # destination directory to save result
    des_dir = src_dir + "/result_multi/" + os.path.basename(folderPath)


    detection.loadVariables()
    detection.getTemperature()
    detection.calculateVariables(refinedResults)
    detection.printResult()
    detection.plotChart(des_dir)
    detection.exportCSV(imageFilename,des_dir)
    detection.CSVtoSqlite3(imageFilename,des_dir)

    #reset variables
    detection.frozen_total = []
    detection.f_ice = []
    detection.frozen_new_nr = []
    detection.frozen_new_ID = []

def reset():
    loadVariables()
    refineWarningData()
    recalulateVariables()
    boundaryCondition.loadVariables()
    boundaryCondition.boundaryCondition(refinedResults)
    oldProgram.loadVariables()
    oldProgram.compareToOldProgram(oldProgramFrozenNewID)
    drawBoundingBox.loadVariables()
    output = drawBoundingBox.getRedBox(refinedResults)
    drawBoundingBox.drawNormalBox(output,"/result_multi/")
    print("done")

# Defining main function to be run
def main():
    loadVariables()
    refineWarningData()
    recalulateVariables()

# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()