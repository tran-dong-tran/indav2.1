# INDA Detection - README #

### Demo-video about functions of software 
https://drive.google.com/file/d/15PAv6TM6hSNj28Tze3X6GROTw_Q1bXWp/view?usp=sharing   

https://youtu.be/s92G7L9MfbI

### Manual for User 
https://drive.google.com/file/d/11ocM9WlSXDAG9EVGFFx0t3231_X3oqBT/view?usp=sharing

### Proof-of-Work
Firstly, read "Manual for User" above to understand why and how red warning box is using for

1. https://drive.google.com/file/d/1tiEQaPiuv4Fj6cNacxW432UTMH0RzcXX/view?usp=sharing
2. https://drive.google.com/file/d/1oCSJaab_wjAE0g77vP0sse3TcMylqS_q/view?usp=sharing
3. https://drive.google.com/file/d/1QzTDAWqqr5wQ66A1kfEPN60gTfPIvgZy/view?usp=sharing
4. https://drive.google.com/file/d/13bwcZOfwy0TkNF65g_b9Bg2Smz6z8Vr3/view?usp=sharing
5. https://drive.google.com/file/d/1er7i4HkYgTlJDCYXCqaM4_z8rN-JJOPz/view?usp=sharing
6. https://drive.google.com/file/d/18PXUlFITCoqFy0KVyBOrZ2_2VhnI8QyX/view?usp=sharing

### Download Repository ###
In Repository https://bitbucket.org/hungnd2905/indav2/src/master/
Click Downloads in the left Menu to download the Repository

### Installation
1. Install Anaconda : https://www.anaconda.com/products/individual#Downloads
   While installing, choose "Register Anaconda3 as my defaul Python 3.8"    
	

1. Install Pycharm  : https://www.jetbrains.com/pycharm/download/
   1. After installed, in Pycharm, choose Button "Add Configuration" in the right above corner
   1. Select "+"
   1. Select "Python"
   1. Select "Script Path" and choose path to current opening .py file
   1. Select "Python interpreter": \current-path-to\anaconda3\python.exe  
   1. Select "Apply", "Ok"


1. In Pycharm : choose "Terminal" bellow, run following commands:  

   	pip install --upgrade pip  
   	pip install progress  
   	pip install opencv-python  

### Run Program
1. Run file mainGUI.py, then "Open Multiple Data" or Run file "gui-yolo-3-multi.py" to select a folder of data to detect
   1. Window is minimized, look at Pycharm in "Run"-Tab at the bottom, the data is detected
   1. After 2 to 3 minutes, this process is done, a result folder is opened, which is in "result_multi" located.
   1. In this folder, there are detected images, CSV Data and graph.png   		    


1. Run file mainGUI.py to show detected results as GUI
   1. Double click on each row, window "Data details" is opened, we can see and edit the content
   1. Choose data on the left list, the image is displayed accordingly
   1. Click on each cell of image to change its state, from frozen -> liquid -> warning
   1. Red warning boxes means: possible new frozen, which is different from result of old program.
   1. Click in button "Update" to recalculate frozen_total, f_ice, frozen_new_nr, frozen_new_ID and save changes in CVS Data, plot new chart, database for GUI 
   1. If you want to revert all previous changes, click "Reset" button and wait for a while to have the original detected data.



### Updates 12.3.2021
1. Feature "Update" with 2 Button "Update" and "Reset" is working

### Updates 14.3.2021
1. Feature for automatic update new frozen by choosing one cell and its position in selection widget is working
2. Updated results are still saved in "result_multi"
3. Remove red warning box for the case of confidence < 96% (because it is not so meaningful)
4. Show new frozen ID of old program and difference to new program in Information Log (for testing purpose)


### Updates 15.3.2021
1. Show Warning boxes at [image number:number of warning boxes] at Information Log. 
2. This information is necessary for searching for images, which have warning boxes and shows how many warning boxes on them.

### Updates 15.4.2021
1. Update final model from Experiment 2


    
	


