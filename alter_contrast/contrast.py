import cv2
import numpy as np


def change_brightness(hsv, value):
    h, s, v = cv2.split(hsv)
    v = cv2.add(v, value)
    v[v > 255] = 255
    v[v < 0] = 0
    return cv2.merge((h, s, v))

def add_noise(img, mean, std):
    noisy_img = img + np.random.normal(mean, std, img.shape)
    clipped_img = np.clip(noisy_img, 0, 255)
    return np.uint8(clipped_img)

if __name__ == '__main__':
    value = 20
    img = cv2.imread('test.jpg')
    img = add_noise(img, 0.0, 10.0)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    hsv = change_brightness(hsv, 20)
    img = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    cv2.imwrite("image_processed.jpg", np.hstack((cv2.imread('test.jpg'), img)))
