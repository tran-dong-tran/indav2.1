import glob

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import sys
from PyQt5 import QtWidgets
from yolo3 import yolo3
import cv2
from PyQt5 import QtCore
from PyQt5.QtGui import QImage, QPixmap
from alter_contrast import contrast
import os
import os.path
import numpy as np
import csv


class MainApp(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.source = QDir.toNativeSeparators(
            QDir.currentPath() + '/INDA_testing_data/20210122_Alginin_Amelie/Alg_1g_l_INDA1')
        self.dest = QDir.toNativeSeparators(
            QDir.currentPath() + '/INDA_testing_data/20210122_Alginin_Amelie/Alg_1g_l_INDA1')
        self.shades = 0
        self.degree = 0
        self.noise = 0
        self.setupUi(self)
        self.image = None
        self.processedImage = None
        self.folderName = ""
        self.chooseButtonSource.clicked.connect(self.chooseSourcePath)
        self.chooseButtonDest.clicked.connect(self.chooseDestPath)
        self.startLabeling.clicked.connect(self.labelData)
        self.calibrationButton.clicked.connect(self.loadCalibrationImage)
        self.indices = []

        for j in range(1, 13):
            for i in ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']:
                self.indices.append(i + str(j))

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(100, 50)
        MainWindow.setMaximumSize(QSize(400, 150))
        self.showMaximized()
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.sourceBox = QLineEdit(self)
        self.sourceBox.setText(self.source)
        self.sourceBox.setReadOnly(True)
        self.sourceBox.move(20, 20)
        self.sourceBox.resize(280, 40)
        self.sourceLabel = QLabel("Source:")

        self.destBox = QLineEdit(self)
        self.destBox.setText(self.dest)
        self.destBox.setReadOnly(True)
        self.destBox.move(20, 20)
        self.destBox.resize(280, 40)
        self.destLabel = QLabel("Destination (leave empty for creating dir in source):")

        self.chooseButtonSource = QPushButton(self.centralwidget)
        self.chooseButtonSource.setText('Choose...')
        self.chooseButtonDest = QPushButton(self.centralwidget)
        self.chooseButtonDest.setText('Choose...')

        self.startLabeling = QPushButton(self.centralwidget)
        self.startLabeling.setLayoutDirection(Qt.LeftToRight)
        self.startLabeling.setObjectName("startAlter")
        self.startLabeling.setText('Start labeling data')

        self.imgLabel = QLabel(self.centralwidget)
        self.imgLabel.setMaximumSize(QSize(940, 800))

        font = QFont()
        font.setPointSize(7)
        font.setBold(False)
        font.setWeight(50)
        self.imgLabel.setFont(font)
        self.imgLabel.setText("")
        self.imgLabel.setObjectName("imgLabel")
        self.horizontalLayout.addWidget(self.imgLabel)

        self.processedLabel = QLabel(self.centralwidget)
        self.processedLabel.setMaximumSize(QSize(940, 800))
        self.processedLabel.setAlignment(Qt.AlignCenter)
        self.processedLabel.setObjectName("processedLabel")

        self.horizontalLayout.addWidget(self.processedLabel)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.verticalLayout.addWidget(self.sourceLabel)
        self.verticalLayout.addWidget(self.sourceBox)
        self.verticalLayout.addWidget(self.chooseButtonSource)

        self.verticalLayout.addWidget(self.destLabel)
        self.verticalLayout.addWidget(self.destBox)
        self.verticalLayout.addWidget(self.chooseButtonDest)

        self.calibrationButton = QPushButton(self.centralwidget)
        self.calibrationButton.setLayoutDirection(Qt.LeftToRight)
        self.calibrationButton.setText('Create calibration file')
        self.verticalLayout.addWidget(self.calibrationButton)

        self.verticalLayout.addWidget(self.startLabeling)
        MainWindow.setCentralWidget(self.centralwidget)

        QMetaObject.connectSlotsByName(MainWindow)

    def chooseSourcePath(self):
        path = QtWidgets.QFileDialog.getExistingDirectory(self, 'Choose Directory with Images to Open',
                                                          str(self.source))
        if path:
            self.source = path
            self.sourceBox.setText(self.source)
            if self.dest == '':
                self.dest = self.source
                self.destBox.setText(self.source)

    def chooseDestPath(self):
        path = QtWidgets.QFileDialog.getExistingDirectory(self, 'Choose Directory with Images to Open', str(self.dest))
        if path:
            self.dest = path
            self.destBox.setText(self.dest)

    def loadCalibrationImage(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, 'Choose Image to Open', str(self.source), '*.jpg')
        if fname:
            self.detectCircles(fname)
        else:
            print("Image is not selected")
            QtWidgets.qApp.quit()
            return False

    def detectCircles(self, fname):
        # detect circles in the image by using Hung's sample detection
        result = yolo3(fname[0])
        image = cv2.imread(fname[0])
        [height, width, channel] = image.shape
        f = open(self.source + '/calibration.txt', 'w')
        for column in result:
            for x, y, w, h, _, _, _, _, id in column:
                cv2.putText(image, id, (x - 10, y), cv2.FONT_ITALIC, 0.9, (36, 255, 12), 2)
                cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)
                tupelString = ' '.join(map(str, self.convert([width, height], (x, x + w, y, y + h))))
                f.write(id + ' ' + tupelString + '\n')
        f.close()
        # show the output image
        cv2.imshow("output", np.hstack([image]))
        cv2.waitKey(0)

    def convert(self, size, box):
        dw = 1. / size[0]
        dh = 1. / size[1]
        x = (box[0] + box[1]) / 2.0
        y = (box[2] + box[3]) / 2.0
        w = box[1] - box[0]
        h = box[3] - box[2]
        x = x * dw
        w = w * dw
        y = y * dh
        h = h * dh
        return (x, y, w, h)

    def readCalibration(self):
        obj = {}
        with open(self.source + '/calibration.txt', newline='') as file:
            lines = file.readlines()
            lines = [line.rstrip() for line in lines]

            for line in lines:
                line = line.split(' ', 1)
                obj[line[0]] = line[1]
        return obj

    def labelData(self):
        print('Start labeling...')
        csv_filename = self.source + '/output_checked.csv' if os.path.isfile(
            self.source + '/output_checked.csv') else self.source + '/output.csv'
        with open(csv_filename, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
            spamreader.__next__()
            frozen = []
            coordinates = self.readCalibration()
            for row in spamreader:
                frozen = list(filter(None, frozen + row[6].replace(' ', '').split(',')))
                label = {
                    'file': row[0],
                    'frozen': frozen
                }

                os.chdir(self.dest)
                normalizedName = os.path.splitext(label['file'])[0]
                for file in glob.glob(normalizedName + '*.jpg'):
                    newFileName = os.path.splitext(file)[0] + '.txt'
                    f = open(self.dest + '/' + newFileName, 'w')

                    for index in self.indices:
                        if index in label['frozen']:
                            f.write('1 ' + coordinates[index] + '\n')
                        else:
                            f.write('0 ' + coordinates[index] + '\n')
        print('[DONE]')

    def directoriesValid(self):
        if self.source == '' or self.dest == '':
            print('Directories invalid...')
            return False
        else:
            return True

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()
        if e.key() == QtCore.Qt.Key_F11:
            if self.isMaximized():
                self.showNormal()
            else:
                self.showMaximized()


# Defining main function to be run
def main():
    # Initializing instance of Qt Application
    app = QtWidgets.QApplication(sys.argv)

    # Initializing object of designed GUI
    window = MainApp()

    # Showing designed GUI
    # window.show()

    # Running application
    app.exec_()


# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()
