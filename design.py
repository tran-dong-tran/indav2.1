
# Form implementation generated from reading ui file 'design.ui'
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(100, 50)
        MainWindow.setMaximumSize(QSize(400, 100))
        self.showMaximized()
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.imgLabel = QLabel(self.centralwidget)
        self.imgLabel.setMaximumSize(QSize(940, 800))
        font = QFont()
        font.setPointSize(7)
        font.setBold(False)
        font.setWeight(50)
        self.imgLabel.setFont(font)
        self.imgLabel.setText("")
        self.imgLabel.setObjectName("imgLabel")
        self.horizontalLayout.addWidget(self.imgLabel)
        self.processedLabel = QLabel(self.centralwidget)
        self.processedLabel.setMaximumSize(QSize(940, 800))
        self.processedLabel.setAlignment(Qt.AlignCenter)
        self.processedLabel.setObjectName("processedLabel")
        self.horizontalLayout.addWidget(self.processedLabel)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.loadButton = QPushButton(self.centralwidget)
        self.loadButton.setMaximumSize(QSize(200, 500))
        self.loadButton.setLayoutDirection(Qt.LeftToRight)
        self.loadButton.setObjectName("loadButton")
        self.verticalLayout.addWidget(self.loadButton, alignment=Qt.AlignHCenter | Qt.AlignVCenter)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "YOLO"))
        self.loadButton.setText(_translate("MainWindow", "Load Image"))
