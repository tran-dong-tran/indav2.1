import time
from pathlib import Path
import pickle
import cv2
import shutil
import os
from storage import storeWarningBox
from storage import storeResultsWarning
root = Path(".")

difference = []
imagePath = []
results = []
folderPath = ""
#list of list [i,j,k]:
#i:image i has red pos
# j is column and k row in image i
warningBox =[]
results_Warning = []


def loadVariables():
    global difference
    global imagePath
    global folderPath
    global results
    global results_Warning

    # load difference between old and new program
    inputFile = 'difference.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    difference = pickle.load(fd)

    # load imagePath
    inputFile = 'imagePath.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    imagePath = pickle.load(fd)

    # load folderPath
    inputFile = 'folderPath.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    folderPath = pickle.load(fd)

    # load results
    inputFile = 'results.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    results = pickle.load(fd)
    results_Warning = results

def getRedBox(input):
    global difference
    global results_Warning
    global warningBox
    # Red box for the difference with old result
    #compare to result of old program, change class to warning if different
    for i in range(len(difference)):
        if difference[i]:
            for x in range(len(difference[i])):
                for j in range(len(input[i])):
                    for k in range(len(input[i][j])):
                        if input[i][j][k][8] == difference[i][x]:
                            temp =[]
                            temp.append(i)
                            temp.append(difference[i][x])

                            #change class to 2
                            warning_list = list(input[i][j][k])
                            warning_list[4] = 2
                            input[i][j][k] = tuple(warning_list)
                            warningBox.append(temp)

    #change class to warning if confidences rate < 0.96
    """
    for i in range(len(input)):
        for j in range(len(input[i])):
            for k in range(len(input[i][j])):
                if input[i][j][k][4] == 0 and input[i][j][k][5] < 0.96:
                    # change class to 2
                    warning_list = list(input[i][j][k])
                    warning_list[4] = 2
                    input[i][j][k] = tuple(warning_list)
    """

    results_Warning = input
    storeWarningBox(warningBox)
    storeResultsWarning(results_Warning)
    return results_Warning

#draw bounding box for one image
def draw_bounding_box(im, input):

    for i in range(len(input)):
        for j in range(len(input[i])):

            # class = 0 (liquid)
            if input[i][j][4] == 0:
                #green
                color_box_current = [0, 255, 255]

            #class = 1  (frozen)
            if input[i][j][4] == 1:
                #blue
                color_box_current = [255, 0, 255]

            #class = 2  (warning)
            if input[i][j][4] == 2:
                #red
                color_box_current = [0, 0, 255]


            xmin = input[i][j][0]
            ymin = input[i][j][1]
            boxwidth = input[i][j][2]
            boxheight = input[i][j][3]

            cv2.rectangle(im, (xmin, ymin), (xmin + boxwidth, ymin + boxheight), color_box_current, 2)


#draw normal bounding box
def drawNormalBox(input,destination):
    global folderPath
    global imagePath
    for i in range(len(imagePath)):
        #remove prefix "Error_"
        img_path = imagePath[i]
        if "Error_" in img_path:
            img_path = img_path.replace("Error_","")

        img = cv2.imread(img_path, cv2.IMREAD_COLOR)

        draw_bounding_box(img, input[i])

        # save detected image to destination folder
        src_dir = os.getcwd()
        des_dir = src_dir + destination + os.path.basename(folderPath)
        new_dst_file_name = os.path.join(des_dir, os.path.basename(imagePath[i]))
        cv2.imwrite(new_dst_file_name, img)


# Defining main function to be run
def main():
    loadVariables()
    output = getRedBox(results_Warning)
    #print(warningBox)
    drawNormalBox(output,"/result_multi/")

# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()
